synthetics
==========

![logo.png](logo.png)

A mockup `docker-compose` stack running `nginx`, `ngrok` and a curl-based `Datadog Synthetics` API client.

## description

- A `synthetics` container creates Datadog Synthetics Tests
- These tests target the `ngrok` tunnel's public URL
- The request is then forwarded to `nginx`

### credentials

A sample credential exists under `env/default`.

To configure the credentials for your `dev` Datadog account, run the following :

    cp `env/default` `env/dev`

Doing so will prevent any future accidental commits & leak of your credentials.

### usage

To run the `synthetics` container vs. your `dev` Datadog account, run the following :

    ENV=dev docker-compose up --build --remove-orphans
