#!/bin/sh
set -euo pipefail

export repo=${repo:-data/repo}

word()      { shuf -n 1 cracklib-small                       ; }
diceware()  { echo $(word)-$(word)-${RANDOM}                 ; }
jq()        { /usr/bin/jq -e ${@}                            ; }
yq()        { /usr/bin/yq eval -oj | jq -e ${@}              ; }
git()       { PAGER=cat /usr/bin/git -C ${repo} ${@}         ; }
tpl()       { source template/${1:-default}.yml | yq ${2:-.} | tee -a ${repo}/$( now ).${1:-default}.tpl.json ; }
now()       { date -u "+%s%N" ; }
timestamp() { date -u "+%Y%m%dT%H%M%SZ" ; }
git_init()  { mkdir -p ${repo} ; git status &>/dev/null || ( git config --global init.defaultBranch "master" && git init && git config --local user.email "you@example.com" && git config --local user.name "Your Name" ) ; }
git_commit(){ git add -A &>/dev/null ; git commit -m "$( now )" &>/dev/null ; git log --pretty=oneline --abbrev-commit -n 1 ; }
git_rm()    { rm -fr $repo/.git $repo/*         ; }
latest()    { ls -t1 ${repo} | grep [0-9].json | head -1        ; }
response()  { cat ${repo}/$( latest ) | jq ${@} ; }
commit_id() { ( git log --pretty=oneline --abbrev-commit -n 1 2>/dev/null | cut -d' ' -f1 ) || echo null ; }

format() {
    cat << HEREDOC
{
  "http":
  {
    "status":%{http_code},
    "scheme":"%{scheme}",
    "url":"%{url_effective}",
    "timestamp":"$( timestamp )",
    "redirect_url":"%{redirect_url}",
    "remote_ip":"%{remote_ip}",
    "version":%{http_version},
    "ssl_verify_result":%{ssl_verify_result},
    "proxy_ssl_verify_result":%{proxy_ssl_verify_result}
  }
}
HEREDOC
}

curl() {
    /usr/bin/curl                                                   \
        --silent                                                    \
        --write-out "\n$( format )\n"                               \
        --user-agent "dd-api/1.0"                                   \
        -H "Content-Type: application/json"                         \
        -H "DD-API-KEY: ${DD_API_KEY}"                              \
        -H "DD-APPLICATION-KEY: ${DD_APPLICATION_KEY}"              \
        "https://api.datadoghq.${DD_SITE:-com}/api/v${v:-1}/${@}" | \
    jq                                                            | \
    tee -a ${repo}/$( now ).json                                  | \
    jq
    git_commit
}

sleep()    { /bin/sleep infinity ; }
validate() { curl validate ; }
list()     { curl synthetics/tests ; }
create()   { curl synthetics/tests/api     -d "$( tpl synthetics )" ; }
trigger()  { curl synthetics/tests/trigger -d "$( tpl trigger )"    ; }
default()  { git_rm && git_init && validate && create && trigger    ; }

## TODO : implement synthetic test DELETE delete()

echo "action=${action}"
case ${action} in
    default|git_rm|git_init|validate|list|create|trigger|sleep|true) ${action} ;;
    *) false ;;
esac
